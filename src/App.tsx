import { BrowserRouter } from "react-router-dom";
import "./App.css";
import { Cabecera } from "./Componentes/Cabecera";
//import { Ruteo } from "./Rutas/Ruteo";

function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <Cabecera/>
        
      </BrowserRouter>
    </div>
  );
}

export default App;
